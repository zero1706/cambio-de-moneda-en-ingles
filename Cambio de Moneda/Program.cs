﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cambio_de_Moneda
{
    class Program
    {
        static void Main(string[] args)
        {


            //primer parametro es la cantidad en string
            //segundo parametro es si queremos que sea mayuscula
            //tercer parametro la moneda
            //Limite 999999999.99
            string resultado = Moneda.Convertir("999999999.99", true, "DOLARS");
            Console.WriteLine(resultado);
            return;
        }
    }
}
